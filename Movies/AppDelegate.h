//
//  AppDelegate.h
//  Movies
//
//  Created by Vipin on 21/04/17.
//  Copyright © 2017 Vipin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

