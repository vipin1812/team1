//
//  MovieTableViewCell.h
//  Movies
//
//  Created by Neeraj Raghavendra on 4/21/17.
//  Copyright © 2017 Vipin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"

@interface MovieTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *movieImage;
@property (weak, nonatomic) IBOutlet UILabel *moviewTitle;
@property (weak, nonatomic) IBOutlet EDStarRating *movieRating;

@end
