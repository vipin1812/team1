//
//  MovieModel.h
//  Movies
//
//  Created by Vipin on 21/04/17.
//  Copyright © 2017 Vipin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MovieModel : NSObject

@property(nonatomic, strong)NSString *movieName, *movieRating, *posterURL, *date;


@end
