//
//  Theatre.m
//  Movies
//
//  Created by Neeraj Raghavendra on 4/21/17.
//  Copyright © 2017 Vipin. All rights reserved.
//

#import "Theatre.h"

@implementation Theatre
@synthesize title,locationName,coordinate;

-(void)init:(NSString*)title locationName:(NSString*)locationName AndCoordinate:(CLLocationCoordinate2D)coordinate{
    self.title = title;
    self.locationName = locationName;
    self.coordinate = coordinate;
    [MKPinAnnotationView redPinColor];
}

@end
