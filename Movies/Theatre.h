//
//  Theatre.h
//  Movies
//
//  Created by Neeraj Raghavendra on 4/21/17.
//  Copyright © 2017 Vipin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface Theatre : NSObject<MKAnnotation>
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *locationName;
@property (assign) CLLocationCoordinate2D coordinate;

-(void)init:(NSString*)title locationName:(NSString*)locationName AndCoordinate:(CLLocationCoordinate2D)coordinate;
@end
