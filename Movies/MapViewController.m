//
//  MapViewController.m
//  Movies
//
//  Created by Neeraj Raghavendra on 4/21/17.
//  Copyright © 2017 Vipin. All rights reserved.
//

#import "MapViewController.h"
#import <MapKit/MapKit.h>

@interface MapViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *moviewMapView;
@property (assign)CLLocationDistance regionRadius;
@property (strong,nonatomic) NSMutableArray *movies;
@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    CLLocation *initialLocation = [[CLLocation alloc]initWithLatitude:21.282778 longitude:-157.829444];
    [self centerMapOnLocation:initialLocation];
    
    
}

-(void) centerMapOnLocation:(CLLocation *)location{
    MKCoordinateRegion coordinateRegion = MKCoordinateRegionMakeWithDistance([location coordinate], _regionRadius * 2.0,_regionRadius * 2.0);
    [_moviewMapView setRegion:coordinateRegion];
}

-(void)loadInitialData{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
