//
//  ViewController.h
//  Movies
//
//  Created by Vipin on 21/04/17.
//  Copyright © 2017 Vipin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import "Globals.h"
#import "MovieModel.h"
#import "MovieTableViewCell.h"

@interface ViewController : UIViewController<NSURLSessionDelegate>
{
    NSMutableArray *moviesArray;
    
}
@property(nonatomic, strong)IBOutlet UITableView *moviesTableView;
@end

