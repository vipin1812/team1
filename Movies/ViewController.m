//
//  ViewController.m
//  Movies
//
//  Created by Vipin on 21/04/17.
//  Copyright © 2017 Vipin. All rights reserved.
//

#import "ViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //NSError *error;
    //[[self moviesTableView] registerClass:[MovieTableViewCell class] forCellReuseIdentifier:@"myCell"];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:MOVIES_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSMutableDictionary *jsonResponse=[NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        NSLog(@"data %@",jsonResponse);
        
        if (!error)
        {
            
            //   NSLog(@"json response is %@",jsonResponse);
            
            //json parsing will be done only when receiving data from server
            if ([[jsonResponse objectForKey:@"Response"] isEqualToString:@"True"])
            {
                moviesArray = [jsonResponse objectForKey:@"Search"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                        // Update your UI
                        
                    [_moviesTableView reloadData];
                });
                
            }
        }
    }];
    
    [postDataTask resume];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [moviesArray count];//count number of row from counting array hear cataGorry is An Array
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"myCell";
    NSLog(@"cell");
    MovieTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        
        cell = [[MovieTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    NSLog(@"data %@",[[moviesArray objectAtIndex:indexPath.row]valueForKey:@"Title"]);
    
    cell.moviewTitle.text = [[moviesArray objectAtIndex:indexPath.row]valueForKey:@"Title"];
    [cell.movieImage sd_setImageWithURL:[NSURL URLWithString:[[moviesArray objectAtIndex:indexPath.row]valueForKey:@"Poster"]] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
